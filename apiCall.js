$(document).ready(
	 function() 
	     {
	     	$("#result").hide();
	     	$(".alert").hide();
		 });



 function drawMap(coordinates) {
 	console.log(coordinates);
  var myLatLng = {lat: coordinates.lat, lng: coordinates.lng};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
   
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
   
  });
}

 function callApi()
    {	
    		$(".alert").fadeOut();
	 	   	 var zipCode = $("#zipCode").val();	
	 	   		 	 
	 	   	 $.ajax({

				  method: "POST",
				  url: "https://maps.googleapis.com/maps/api/geocode/json?address="+zipCode+"&region=fr",
				  dataType: "json",
				  success: function(response)
				    {
				    	
				    	if(response.results.length == 0)
				    	   $('.alert').fadeIn(2000);
				    	 else
				    	 {
				    	 	result = response.results[0].formatted_address;
				    	 	result= result.split(" ")[1];
				    	 	
				    	 	if( result.endsWith(",") )
				    	 		{	
				    	 			
				    	 			coordinates =  response.results[0].geometry.location;
				    	 			drawMap(coordinates);
				    	 			$("#city").text(result.substring(0,result.length-1) );

				    	 			$("#result").fadeIn();

				    	 		}
				    	 	  else
				    	 	  	 $('.alert').fadeIn(2000);

				    	 	  	
				    	 }
				    	
				    	
				    }
			});
	 	
 	}
